#!/usr/bin/env bash

function containerShell() {
    if [ "$2" == "" ]; then
        sudo docker exec -it "$1" sh
    elif [ "$2" == "logs" ]; then
        sudo docker logs "$1" --tail=50 -f
    elif [ "$2" == "rm" ]; then
        sudo docker rm "$1" -f
    else
        # restart|stop
        sudo docker "$2" "$1"
    fi
}

if [ "$1" == "up" ]; then
    sudo docker-compose up -d
elif [ "$1" == "down" ]; then
    sudo docker-compose down
elif [ "$1" == "ps" ]; then
    sudo docker ps -a
else
    containerShell "senseyedeveloper_backend_local" "$1"
fi
