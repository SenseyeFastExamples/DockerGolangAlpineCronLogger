package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Success running!"))
	})

	log.Println("Server start on port 8888")
	log.Fatal(http.ListenAndServe(":8888", nil))
}
